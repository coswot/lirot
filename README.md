
# LiRoT
LiRoT is a lightweight incremental reasoner for the Web of Things. It is written in C, and designed to run on platforms with limited computational capabilities. It currently supports the RDFS ruleset (more rulesets will be added in the future).

Some functions in LiRoT include named graph parameters; quads are not supported yet during the reasoning process, but will be in the future.

## Setup and dependencies

To install LiRoT in a Linux environment, one first needs to install its dependencies. These dependencies are ```git``` and ```cmake```. The installation procedure depends on one's Linux distribution. On a Ubuntu setup, this would be:

```bash
sudo apt-get install git cmake
```

Once the dependencies are installed, open a terminal and clone the LiRoT repository:

```bash
git clone https://gitlab.com/coswot/lirot
```

Then enter the LiRoT directory:

```bash
cd lirot
```

Now download LiRoT submodules:

```bash
git submodule update --init --recursive
```

Now LiRoT can be tested directly with the provided demo (see the following section) or as a library (see the ```How to use LiRoT as a library?``` section).

## A simple demo

The code presented in this tutorial can be found in a demo ([```demo```](demo/) directory, in which a tutorial is also available to launch the demo). 

This demo creates an instance of LiRoT, with a specific ruleset and an initial data file (the LUBM ontology in this case). Then, it inserts explicit triples in the reasoner from a data file. Reasoning is automatically triggered. Finally, it searches for the triples where the subject is of type Professor. Please check the Readme file in the directory ```demo``` for more details.

## How to use LiRoT as a library?

LiRoT can be used as a CMake dependency (the library is called `lirot`). Here is an example of a `main` function using LiRoT. First include the `KBWrapper.h` file:

```c
#include "lirot/KBWrapper.h"

int main(int argc, char* argv[])
{
	return 0;
}
```
All further code will be added to the ```main``` function.

To create a LiRoT instance, one must use the ```APIInitKBWrapper``` function. This function has two parameters: an ontology file path and a ruleset file path. In this example we use the LUBM ontology and the RDFS-Default ruleset (a subset of RDFS). The function call is then:

```c
KBWrapper* lirot = APIInitKBWrapper("../demo/data/univ-bench.ttl", "../rulesets/rdfs-default.txt");
```
Explicit facts can be added to the LiRoT instance that was just created using the ```APIAddFactsFromText``` and ```APIAddFactsFromFile``` functions (depending whether data is in a character string or a file; in both cases written in Turtle format). Here, we add a small amount of data contained in a string, then data from a LUBM file. Reasoning is 
triggered with every explicit fact insertion, hence implicit facts inferred from the ontology, explicit facts and the selected ruleset will automatically be inserted.
```APIAddFactsFromText``` (resp.  ```APIAddFactsFromText```) takes a LiRoT instance and the data string (resp. data file path) as parameters. File paths can be relative or absolute.

```c
char* data = "@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\
	      @prefix :	    <http://example.com#> .\
	      @prefix owl:  <http://www.w3.org/2002/07/owl#> .\
	      :LiRoT a owl:Thing .";

APIAddFactsFromText(lirot, data);
APIAddFactsFromFile(lirot, "../demo/data/additional-data.ttl");
```

Here, LiRoT parses and stores explicit facts written in Turtle (in a data string and from a file), then uses them to derive new implicit facts.

To perform a match operation on the LiRoT internal knowledge base, the ```APIMatchFact``` function is used. In this example, we match all triples where the subject is of type ```Professor```.
```APIMatchFact``` takes a LiRoT instance, a subject, a predicate, an object and a graph as parameters. The latter three parameters can either be a URI, a blank node, a literal, or ```NULL``` if they are variables in the match operation; this is typically used for the graph parameter, as named graphs are not always used.
```APIMatchFact``` returns in a string the matching triples or quads in the LiRoT instance, in the Turtle format.

```c
char* matches = APIMatchFact(lirot, NULL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor", NULL);
printf("%s\n", matches); // prints (in Turtle format) all triples for which the subject is of type Professor
```
One can also check the existence of a specific fact. This can be done using the ```APIHasFact``` function. It takes a LiRoT instance, a subject, a predicate, on object and a graph (the latter can be left ```NULL```). Here is an example:
```c
bool exists = APIHasFact(lirot, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Chair", "http://www.w3.org/2000/01/rdf-schema#subClassOf", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor", NULL);
printf("Fact exists: %d\n", exists); // prints 1 if the fact is present in the LiRoT instance; prints 0 otherwise.
```
The returned value is a boolean (true if the fact is present in the LiRoT instance, false otherwise).

Facts can also be deleted from a LiRoT instance using the ```APIDeleteFactsFromText``` or ```APIDeleteFactsFromFile``` functions. Implicit facts derived from the deleted facts are deleted as well, if they cannot be derived another way.
```APIDeleteFactsFromText``` (resp. ```APIDeleteFactsFromFile```) takes two parameters: a LiRoT instance, and a data string (resp. data file path). File paths can be relative or absolute. Here is an example:

```c
char* data = "@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\
	      @prefix :	    <http://example.com#> .\
	      @prefix owl:  <http://www.w3.org/2002/07/owl#> .\
	      :LiRoT a owl:Thing .";

APIDeleteFactsFromText(lirot, data);
APIDeleteFactsFromFile(lirot, "../demo/data/additional-data.ttl");
```
Do not redefine the ```data``` string variable if already used with the ```APIAddFactsFromText``` function.

Finally, to free all memory space allocated to a LiRoT instance, the ```APIDestroyKBWrapper``` can be used:

```c
APIDestroyKBWrapper(lirot); // here, lirot is of type KBWrapper
```

## Compiling LiRoT

LiRoT is built as a CMake library. To compile the code presented in this tutorial, in the root folder run the following commands:
```bash
mkdir build
cd build
cmake ..
make
```

## Where to find the documentation?

We also provide full code documentation in the ```doc``` folder (open file ```doc/html/index.html``` in a web browser).

## License

LiRoT is available under the [CeCILL-C](http://www.cecill.info/licences.en.html) license.
