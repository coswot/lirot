var searchData=
[
  ['addexplicitfacts_0',['addExplicitFacts',['../Rete_8c.html#a14909c151f4894823e4fbcbf01938993',1,'addExplicitFacts():&#160;Rete.c'],['../structRete.html#a14909c151f4894823e4fbcbf01938993',1,'Rete::addExplicitFacts()']]],
  ['addfact_1',['addFact',['../structFactLinkedList.html#a5d2fd1e7e3099244df9883914c2b9ebb',1,'FactLinkedList::addFact()'],['../FactLinkedList_8c.html#a5d2fd1e7e3099244df9883914c2b9ebb',1,'addFact():&#160;FactLinkedList.c']]],
  ['alphaleftpredecessor_2',['alphaLeftPredecessor',['../structBetaNode.html#a842f7d22589d6e10c2a37904fc5ba9fd',1,'BetaNode']]],
  ['alphanode_3',['AlphaNode',['../structAlphaNode.html',1,'AlphaNode'],['../AlphaNode_8h.html#a9bd4fd7732c0033a72e15f0995bad289',1,'AlphaNode():&#160;AlphaNode.h']]],
  ['alphanode_2ec_4',['AlphaNode.c',['../AlphaNode_8c.html',1,'']]],
  ['alphanode_2eh_5',['AlphaNode.h',['../AlphaNode_8h.html',1,'']]],
  ['alphanodematchfact_6',['alphaNodeMatchFact',['../AlphaNode_8h.html#a44797a05cd18d90383190367b60b7884',1,'alphaNodeMatchFact(AlphaNode *alpha, Fact *f):&#160;AlphaNode.c'],['../AlphaNode_8c.html#a44797a05cd18d90383190367b60b7884',1,'alphaNodeMatchFact(AlphaNode *alpha, Fact *f):&#160;AlphaNode.c']]],
  ['alphanodes_7',['alphaNodes',['../structRete.html#a06f8d2943ab3aa61bbe5413e3fd9b52f',1,'Rete']]],
  ['apiaddfact_8',['APIAddFact',['../structKBWrapper.html#a923dcbdc78132a220de401b315e8fd6d',1,'KBWrapper']]],
  ['apiaddfactsfromfile_9',['APIAddFactsFromFile',['../structKBWrapper.html#a7965d5906d69aa87e4822ad3880e2391',1,'KBWrapper::APIAddFactsFromFile()'],['../KBWrapper_8c.html#a7965d5906d69aa87e4822ad3880e2391',1,'APIAddFactsFromFile():&#160;KBWrapper.c']]],
  ['apiaddfactsfromtext_10',['APIAddFactsFromText',['../structKBWrapper.html#a3c2fcdb9df02d0dd5a6241c2c3f3de9e',1,'KBWrapper::APIAddFactsFromText()'],['../KBWrapper_8c.html#a3c2fcdb9df02d0dd5a6241c2c3f3de9e',1,'APIAddFactsFromText():&#160;KBWrapper.c']]],
  ['apideletefact_11',['APIDeleteFact',['../structKBWrapper.html#a79f723a272912aea4fb57edf766e0f64',1,'KBWrapper']]],
  ['apideletefactsfromfile_12',['APIDeleteFactsFromFile',['../KBWrapper_8c.html#a919f981e6a2bfdfddbbb3d87325898bb',1,'APIDeleteFactsFromFile():&#160;KBWrapper.c'],['../structKBWrapper.html#a919f981e6a2bfdfddbbb3d87325898bb',1,'KBWrapper::APIDeleteFactsFromFile()']]],
  ['apideletefactsfromtext_13',['APIDeleteFactsFromText',['../KBWrapper_8c.html#a27ae8e96ba9f7616269af324d8f18e31',1,'APIDeleteFactsFromText():&#160;KBWrapper.c'],['../structKBWrapper.html#a27ae8e96ba9f7616269af324d8f18e31',1,'KBWrapper::APIDeleteFactsFromText()']]],
  ['apidestroykbwrapper_14',['APIDestroyKBWrapper',['../KBWrapper_8c.html#a2bfcef708696f4536494a3a1a2c1303c',1,'APIDestroyKBWrapper():&#160;KBWrapper.c'],['../structKBWrapper.html#a20a82f260baa72ec539508db32649e33',1,'KBWrapper::APIDestroyKBWrapper()']]],
  ['apihasfact_15',['APIHasFact',['../KBWrapper_8c.html#a8345e3c989d202c0abae50cf6235b357',1,'APIHasFact():&#160;KBWrapper.c'],['../structKBWrapper.html#ab3cd742ad4ea53fa0dc6ba4eefc791cc',1,'KBWrapper::APIHasFact(KBWrapper *w, char *subject, char *predicate, char *object, char *graph)']]],
  ['apiinfer_16',['APIInfer',['../structKBWrapper.html#a04888144c98ee98078b7c715e7ad245e',1,'KBWrapper']]],
  ['apiinitkbwrapper_17',['APIInitKBWrapper',['../structKBWrapper.html#add3dc2e04a848d4c77f281205bb24791',1,'KBWrapper::APIInitKBWrapper()'],['../KBWrapper_8c.html#add3dc2e04a848d4c77f281205bb24791',1,'APIInitKBWrapper(char *initialFactFilePath, char *rulesFilePath):&#160;KBWrapper.c']]],
  ['apimatchfact_18',['APIMatchFact',['../KBWrapper_8c.html#a1bfdbf4bf4f25a78b6e9d6a67806177f',1,'APIMatchFact():&#160;KBWrapper.c'],['../structKBWrapper.html#aba4013f0af871e7018ce37af1b2c8872',1,'KBWrapper::APIMatchFact()']]],
  ['apireadfile_19',['APIReadFile',['../KBWrapper_8c.html#a8ae594ca331c2ba6ac589aee2762d11c',1,'APIReadFile(char *filepath):&#160;KBWrapper.c'],['../KBWrapper_8h.html#a8ae594ca331c2ba6ac589aee2762d11c',1,'APIReadFile(char *filepath):&#160;KBWrapper.c']]],
  ['atom_20',['atom',['../structAlphaNode.html#ac0671ef933aa24c0eb2f006429802050',1,'AlphaNode']]]
];
