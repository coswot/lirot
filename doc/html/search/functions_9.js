var searchData=
[
  ['matchatomwithfact_0',['matchAtomWithFact',['../Fact_8c.html#a81d122631ee8d236c1113fe8105f4ce6',1,'matchAtomWithFact(Fact *atom, Fact *f):&#160;Fact.c'],['../Fact_8h.html#a81d122631ee8d236c1113fe8105f4ce6',1,'matchAtomWithFact(Fact *atom, Fact *f):&#160;Fact.c']]],
  ['matchtwoatoms_1',['matchTwoAtoms',['../Fact_8c.html#a938e2741007e59e01df18ee9630419a0',1,'matchTwoAtoms(Fact *a1, Fact *a2):&#160;Fact.c'],['../Fact_8h.html#a938e2741007e59e01df18ee9630419a0',1,'matchTwoAtoms(Fact *a1, Fact *a2):&#160;Fact.c']]],
  ['movealphamatches_2',['moveAlphaMatches',['../AlphaNode_8c.html#ac81f4ac417940ea7722800317b7e1885',1,'moveAlphaMatches(AlphaNode *alpha):&#160;AlphaNode.c'],['../AlphaNode_8h.html#ac81f4ac417940ea7722800317b7e1885',1,'moveAlphaMatches(AlphaNode *alpha):&#160;AlphaNode.c']]],
  ['movebetamatches_3',['moveBetaMatches',['../BetaNode_8c.html#a386d4625905a451e5394a4191a56ca84',1,'moveBetaMatches(BetaNode *beta):&#160;BetaNode.c'],['../BetaNode_8h.html#a386d4625905a451e5394a4191a56ca84',1,'moveBetaMatches(BetaNode *beta):&#160;BetaNode.c']]]
];
