var searchData=
[
  ['addexplicitfacts_0',['addExplicitFacts',['../structRete.html#a14909c151f4894823e4fbcbf01938993',1,'Rete::addExplicitFacts()'],['../Rete_8c.html#a14909c151f4894823e4fbcbf01938993',1,'addExplicitFacts():&#160;Rete.c']]],
  ['addfact_1',['addFact',['../structFactLinkedList.html#a5d2fd1e7e3099244df9883914c2b9ebb',1,'FactLinkedList::addFact()'],['../FactLinkedList_8c.html#a5d2fd1e7e3099244df9883914c2b9ebb',1,'addFact(FactLinkedList *facts, Fact *element):&#160;FactLinkedList.c']]],
  ['alphanodematchfact_2',['alphaNodeMatchFact',['../AlphaNode_8h.html#a44797a05cd18d90383190367b60b7884',1,'alphaNodeMatchFact(AlphaNode *alpha, Fact *f):&#160;AlphaNode.c'],['../AlphaNode_8c.html#a44797a05cd18d90383190367b60b7884',1,'alphaNodeMatchFact(AlphaNode *alpha, Fact *f):&#160;AlphaNode.c']]],
  ['apiaddfact_3',['APIAddFact',['../structKBWrapper.html#a923dcbdc78132a220de401b315e8fd6d',1,'KBWrapper']]],
  ['apiaddfactsfromfile_4',['APIAddFactsFromFile',['../KBWrapper_8c.html#a7965d5906d69aa87e4822ad3880e2391',1,'APIAddFactsFromFile():&#160;KBWrapper.c'],['../structKBWrapper.html#a7965d5906d69aa87e4822ad3880e2391',1,'KBWrapper::APIAddFactsFromFile(KBWrapper *w, char *filename)']]],
  ['apiaddfactsfromtext_5',['APIAddFactsFromText',['../structKBWrapper.html#a3c2fcdb9df02d0dd5a6241c2c3f3de9e',1,'KBWrapper::APIAddFactsFromText()'],['../KBWrapper_8c.html#a3c2fcdb9df02d0dd5a6241c2c3f3de9e',1,'APIAddFactsFromText():&#160;KBWrapper.c']]],
  ['apideletefact_6',['APIDeleteFact',['../structKBWrapper.html#a79f723a272912aea4fb57edf766e0f64',1,'KBWrapper']]],
  ['apideletefactsfromfile_7',['APIDeleteFactsFromFile',['../structKBWrapper.html#a919f981e6a2bfdfddbbb3d87325898bb',1,'KBWrapper::APIDeleteFactsFromFile()'],['../KBWrapper_8c.html#a919f981e6a2bfdfddbbb3d87325898bb',1,'APIDeleteFactsFromFile():&#160;KBWrapper.c']]],
  ['apideletefactsfromtext_8',['APIDeleteFactsFromText',['../structKBWrapper.html#a27ae8e96ba9f7616269af324d8f18e31',1,'KBWrapper::APIDeleteFactsFromText()'],['../KBWrapper_8c.html#a27ae8e96ba9f7616269af324d8f18e31',1,'APIDeleteFactsFromText():&#160;KBWrapper.c']]],
  ['apidestroykbwrapper_9',['APIDestroyKBWrapper',['../structKBWrapper.html#a20a82f260baa72ec539508db32649e33',1,'KBWrapper::APIDestroyKBWrapper()'],['../KBWrapper_8c.html#a2bfcef708696f4536494a3a1a2c1303c',1,'APIDestroyKBWrapper():&#160;KBWrapper.c']]],
  ['apihasfact_10',['APIHasFact',['../structKBWrapper.html#ab3cd742ad4ea53fa0dc6ba4eefc791cc',1,'KBWrapper::APIHasFact()'],['../KBWrapper_8c.html#a8345e3c989d202c0abae50cf6235b357',1,'APIHasFact():&#160;KBWrapper.c']]],
  ['apiinfer_11',['APIInfer',['../structKBWrapper.html#a04888144c98ee98078b7c715e7ad245e',1,'KBWrapper']]],
  ['apiinitkbwrapper_12',['APIInitKBWrapper',['../structKBWrapper.html#add3dc2e04a848d4c77f281205bb24791',1,'KBWrapper::APIInitKBWrapper()'],['../KBWrapper_8c.html#add3dc2e04a848d4c77f281205bb24791',1,'APIInitKBWrapper():&#160;KBWrapper.c']]],
  ['apimatchfact_13',['APIMatchFact',['../structKBWrapper.html#aba4013f0af871e7018ce37af1b2c8872',1,'KBWrapper::APIMatchFact()'],['../KBWrapper_8c.html#a1bfdbf4bf4f25a78b6e9d6a67806177f',1,'APIMatchFact(KBWrapper *w, char *s, char *p, char *o, char *g):&#160;KBWrapper.c']]],
  ['apireadfile_14',['APIReadFile',['../KBWrapper_8c.html#a8ae594ca331c2ba6ac589aee2762d11c',1,'APIReadFile(char *filepath):&#160;KBWrapper.c'],['../KBWrapper_8h.html#a8ae594ca331c2ba6ac589aee2762d11c',1,'APIReadFile(char *filepath):&#160;KBWrapper.c']]]
];
