var indexSectionsWithContent =
{
  0: "abcdfghijklmnoprstuv",
  1: "abfhiklrt",
  2: "abfklruv",
  3: "abcdfghilmprst",
  4: "abcdfhijklmnoprstv",
  5: "abfhiklrt",
  6: "t",
  7: "bluv",
  8: "lt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Pages"
};

