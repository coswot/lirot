var searchData=
[
  ['matchatomwithfact_0',['matchAtomWithFact',['../Fact_8c.html#a81d122631ee8d236c1113fe8105f4ce6',1,'matchAtomWithFact(Fact *atom, Fact *f):&#160;Fact.c'],['../Fact_8h.html#a81d122631ee8d236c1113fe8105f4ce6',1,'matchAtomWithFact(Fact *atom, Fact *f):&#160;Fact.c']]],
  ['matches_1',['matches',['../structAlphaNode.html#a36bc0d439356e9ff0134e7a8cf615c13',1,'AlphaNode::matches()'],['../structBetaNode.html#a3b13db5751bc4b29cae41675125b0769',1,'BetaNode::matches()']]],
  ['matchingalphanodes_2',['matchingAlphaNodes',['../structBetaNode.html#a2abe28e4e55c9d53517edad9afdd080b',1,'BetaNode']]],
  ['matchtwoatoms_3',['matchTwoAtoms',['../Fact_8c.html#a938e2741007e59e01df18ee9630419a0',1,'matchTwoAtoms(Fact *a1, Fact *a2):&#160;Fact.c'],['../Fact_8h.html#a938e2741007e59e01df18ee9630419a0',1,'matchTwoAtoms(Fact *a1, Fact *a2):&#160;Fact.c']]],
  ['meta_4',['meta',['../structTerm.html#a12ec18d20030ec06e21c6e5d34a788df',1,'Term']]],
  ['movealphamatches_5',['moveAlphaMatches',['../AlphaNode_8c.html#ac81f4ac417940ea7722800317b7e1885',1,'moveAlphaMatches(AlphaNode *alpha):&#160;AlphaNode.c'],['../AlphaNode_8h.html#ac81f4ac417940ea7722800317b7e1885',1,'moveAlphaMatches(AlphaNode *alpha):&#160;AlphaNode.c']]],
  ['movebetamatches_6',['moveBetaMatches',['../BetaNode_8c.html#a386d4625905a451e5394a4191a56ca84',1,'moveBetaMatches(BetaNode *beta):&#160;BetaNode.c'],['../BetaNode_8h.html#a386d4625905a451e5394a4191a56ca84',1,'moveBetaMatches(BetaNode *beta):&#160;BetaNode.c']]]
];
