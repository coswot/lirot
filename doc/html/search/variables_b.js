var searchData=
[
  ['nb_5fvariables_0',['nb_variables',['../structBetaMatch.html#adbdcbee1756ebc2ed13677b530da6394',1,'BetaMatch']]],
  ['nb_5fverbose_5fargs_1',['NB_VERBOSE_ARGS',['../Verbose_8c.html#ac2ab2d4fefcc1f646b0fc44c276226ae',1,'NB_VERBOSE_ARGS():&#160;Verbose.c'],['../Verbose_8h.html#ac2ab2d4fefcc1f646b0fc44c276226ae',1,'NB_VERBOSE_ARGS():&#160;Verbose.c']]],
  ['newmatches_2',['newMatches',['../structAlphaNode.html#a378ce8d382082e544e74c72c87fa6cff',1,'AlphaNode::newMatches()'],['../structBetaNode.html#a146b22c6fd901edb9c826e9f5c4f63b8',1,'BetaNode::newMatches()']]],
  ['next_3',['next',['../structFactLinkedListNode.html#ab910a0fd3b73a7ac115827b41fd051c3',1,'FactLinkedListNode::next()'],['../structLinkedListNode.html#a4b3e9f12adc3e97514ad99be4069dc33',1,'LinkedListNode::next()']]]
];
