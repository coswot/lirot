#include "lirot/KBWrapper.h"

int main(int argc, char* argv[])
{
    if (argc == 4)
    {
        /*
        Create an instance of LiRoT, with a specific ruleset and an initial data file (here we use the LUBM ontology).
        */
        KBWrapper* wrapper = APIInitKBWrapper(argv[2], argv[1]);

        /*
        Insert new explicit triples in the reasoner. Reasoning is automatically triggered.
        */
        APIAddFactsFromFile(wrapper, argv[3]);

        /*
        Retrieve all triples matching a certain pattern.
        Here we look for all resources of type "Professor".
        */
        char* matches = APIMatchFact(wrapper, NULL, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor", NULL);
        printf("%s\n", matches);
        free(matches);

        /*
        Check if a specific triple exists.
        */
        bool exists = APIHasFact(wrapper, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Chair", "http://www.w3.org/2000/01/rdf-schema#subClassOf", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor", NULL);
        printf("Fact exists: %d\n", exists);
        /*
        Remove explicit triples from the reasoner. Implicit triples that depend on them are automatically removed.
        */
        APIDeleteFactsFromFile(wrapper, argv[3]);

        /*
        Free the dynamic memory allocated to the reasoner.
        */
        APIDestroyKBWrapper(wrapper);
    }
    else
    {
        printf("To use: ./demo <ruleset-file> <ontology-file> <dataset-file>\n");
    }

	return 0;
}
