# demo

### Setup and run the demo

To compile and run the demo, in this directory run the following commands:

```bash
cd ..
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make
./demo ../rulesets/rdfs-default.txt ../demo/data/univ-bench.ttl ../demo/data/additional-data.ttl
```

### Description

This demo creates an instance of LiRoT, with a specific ruleset and an initial data file (the LUBM ontology in this case). Then, it inserts explicit triples in the reasoner from a data file. Reasoning is automatically triggered. Finally, it searches for the triples where the subject is of type Professor. It outputs the following result, which in turn are inferred facts after reasoning:

```
<http://www.Department14.University0.edu/FullProfessor6>
     a <http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor> .

<http://www.Department5.University0.edu/AssistantProfessor0>
     a <http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor> .

<http://www.Department9.University0.edu/FullProfessor1>
     a <http://swat.cse.lehigh.edu/onto/univ-bench.owl#Professor> .
```

Then it looks for a specific fact, and displays whether it is present in the knowledge base of the reasoner (it is, in this case).

This demo uses the RDFS-Default ruleset, but three rulesets are available (RDFS-Simple, RDFS-Default, RDFS-Full).
