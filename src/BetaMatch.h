#ifndef BETAMATCH_H_INCLUDED
#define BETAMATCH_H_INCLUDED

#include "Fact.h"
#include "FactLinkedList.h"

/**
 * Structure containing all the values for each variable in a beta node.
 * Convention: first, join variables are added to the array, then other variables (using the same order as described in the corresponding BetaVariables).
 * 
 * Example: for join variables
 *   - ?x = ex:alcon
 *   - ?w = ex:Engine
 *
 * Other variables:
 *   - ?v = ex:hasEngine
 *   - ?p = ex:hasComp
 *   - ?u = ex:myCar
 *
 * \code{.c}
 * BetaMatch
 * {
 *   variables: {"ex:alcon", "ex:Engine", "ex:hasEngine", "ex:hasComp", "ex:myCar"};
 *   nb_variables: 5
 * }
 * \endcode
 */
typedef struct BetaMatch
{
    /**
     * Values for each variable included in the beta node.
     * Convention: join variables are entailed first, then other variables.
     */
    Term** variables;

    /**
     * Total number of variables considered in the beta node.
     */
    int nb_variables;

    Fact* leftFactCause;

    struct BetaMatch* leftMatchCause;

    Fact* rightCause;
} BetaMatch;

BetaMatch* createBetaMatch(int nbVariables);

bool betaMatchEquals(BetaMatch* m1, BetaMatch* m2);

bool betaMatchContainsOrigin(BetaMatch* m, Fact* f);

void destroyBetaMatch(BetaMatch* m);

#endif // !BETAMATCH_H_INCLUDED