#include "Rule.h"

LinkedList* getRules(LinkedList* lines)
{
	LinkedList* rules = createLinkedList();
	char ruleDelim[] = ";";
	char factDelim[] = ",";

	LinkedListNode* currLine = lines->first;
	while (currLine != NULL)
	{
		LinkedList* atoms = splitStr((char*)currLine->data, ruleDelim);
		FactLinkedList* body = createFactLinkedList();
		LinkedListNode* currAtom = atoms->first;
		int i = 0;
		while (i < atoms->size - 1)
		{
			Fact* a = getFactFromAtom((char*)currAtom->data, true);
			addFact(body, a);

			currAtom = currAtom->next;
			i++;
		}

		Fact* head = getFactFromAtom((char*)currAtom->data, true);
		Rule* r = createRule(body, head);
		pushLinkedList(rules, r);

        // Free atoms copies
        currAtom = atoms->first;
        while (currAtom != NULL)
        {
            free((char*)currAtom->data);
            currAtom = currAtom->next;
        }
        freeLinkedList(atoms);

        
        freeFactLinkedList(body);

		currLine = currLine->next;
	}

	return rules;
}

Rule* createRule(FactLinkedList* body, Fact* head)
{
    Rule* result = (Rule*)malloc(sizeof(Rule));
    if (result)
    {
        result->head = head;
        result->body = createFactLinkedList();
        FactLinkedListNode* curr = body->first;
        while (curr != NULL)
        {
            addFact(result->body, curr->data);
            curr = curr->next;
        }
    }

    if (verboseOn)
    {
        printf("Created rule ");
        printRule(result);
    }

    return result;
}

void printRule(Rule* r)
{
    printf("Rule\n{\n");
    printf("    Body\n    {\n");

    FactLinkedListNode* currBody = r->body->first;
    while (currBody != NULL)
    {
        printFact(currBody->data);
        currBody = currBody->next;
    }

    printf("    }\n");
    printf("}\n");
}

void destroyRule(Rule *r)
{
    FactLinkedListNode* currFact = r->body->first;
    while (currFact != NULL)
    {
        destroyFact(currFact->data, true, true);
        currFact = currFact->next;
    }

    freeFactLinkedList(r->body);

    destroyFact(r->head, true, true);
    free(r);
}