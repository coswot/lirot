#include "FactLinkedList.h"

FactLinkedList* createFactLinkedList()
{
	FactLinkedList* l = (FactLinkedList*)malloc(sizeof(FactLinkedList));
	if (l)
	{
		l->size = 0;
		l->first = NULL;
		l->last = NULL;
	}
	

	return l;
}

void addFact(FactLinkedList* facts, Fact* element)
{
	FactLinkedListNode* newElem = (FactLinkedListNode*)malloc(sizeof(FactLinkedListNode));
	if (newElem)
	{

		newElem->next = NULL;
		newElem->data = element;
		if (facts->size == 0)
		{
			facts->first = newElem;
			facts->last = facts->first;
		}
		else
		{
			facts->last->next = newElem;
			facts->last = newElem;
		}
	}
	
	facts->size++;
}

void insertFactLinkedListAtIndex(FactLinkedList* l, Fact* element, int index)
{
	if (index == 0)
	{
		FactLinkedListNode* newElem = (FactLinkedListNode*)malloc(sizeof(FactLinkedListNode));
		if (newElem)
		{
			newElem->data = element;
			newElem->next = l->first;
			l->first = newElem;
			l->size++;
		}
	}
	else if (index > 0)
	{
		if (index == l->size)
		{
			addFact(l, element);
		}
		else if (index < l->size)
		{
			int i = 0;
			FactLinkedListNode* current = l->first;
			while (i < index - 1)
			{
				current = current->next;
				i++;
			}
			FactLinkedListNode* newElem = (FactLinkedListNode*)malloc(sizeof(FactLinkedListNode));
			if (newElem)
			{
				newElem->data = element;
				newElem->next = current->next;
				current->next = newElem;
				l->size++;
			}
		}
	}
}

Fact* hasFact(FactLinkedList* facts, Fact* element)
{
	FactLinkedListNode* curr = facts->first;
	while (curr != NULL)
	{
		if (factEquals(curr->data, element))
		{
			return curr->data;
		}
		curr = curr->next;
	}

	return NULL;
}

void* getFactLinkedListAtIndex(FactLinkedList* l, int index)
{
	Fact* res = NULL;

	if (index >= 0 && index < l->size)
	{
		FactLinkedListNode* curr = l->first;
		int i = 0;
		while (i < index)
		{
			curr = curr->next;
			i++;
		}

		res = curr->data;
	}

	return res;
}

void removeFactLinkedListAtIndex(FactLinkedList* l, int index)
{
	if (index < l->size)
	{
		if (index == 0)
		{
			FactLinkedListNode* toRemove = l->first;
			l->first = l->first->next;
			free(toRemove);
			l->size--;
		}
		else
		{
			FactLinkedListNode* current = l->first;
			int i = 0;
			while (i < index - 1)
			{
				current = current->next;
				i++;
			}
			FactLinkedListNode* toRemove = current->next;
			current->next = current->next->next;
			free(toRemove);
			l->size--;
		}
	}

	if (l->size == 1)
	{
		l->last = l->first;
	}

	if (l->size == 0)
	{
		l->first = NULL;
		l->last = NULL;
	}
}

void freeFactLinkedList(FactLinkedList* l)
{
	FactLinkedListNode* current = l->first;
	FactLinkedListNode* tmp;

	while (current != NULL)
	{
		tmp = current;
		current = current->next;
		free(tmp);
	}
	
	free(l);
}

FactLinkedList* getFacts(LinkedList* lines)
{
	FactLinkedList* facts = createFactLinkedList();

	LinkedListNode* currLine = lines->first;
	while (currLine != NULL)
	{
		Fact* f = getFactFromAtom((char*)currLine->data, false);
		addFact(facts, f);

		currLine = currLine->next;
	}

	return facts;
}

void deleteFact(FactLinkedList* facts, Fact* element)
{
	FactLinkedListNode* curr = facts->first;
	while (curr != NULL)
	{
		if (factEquals(curr->data, element))
		{
			if (curr != facts->last)
			{
				if (curr == facts->first)
				{
					facts->first = facts->first->next;
					free(curr);
					facts->size--;
					break;
				}
				else
				{
					FactLinkedListNode* prec = facts->first;
					while (prec->next != curr)
					{
						prec = prec->next;
					}
					prec->next = prec->next->next;
					free(curr);
					facts->size--;
					break;
				}
			}
			else
			{
				if (facts->size == 1)
				{
					facts->first = NULL;
					facts->last = NULL;
					facts->size = 0;
				}
				else
				{
					FactLinkedListNode* prec = facts->first;
					while (prec->next != curr)
					{
						prec = prec->next;
					}
					prec->next = NULL;
					facts->last = prec;
					free(curr);
					facts->size--;
				}
				
				break;
			}

			if (facts->size == 1)
			{
				facts->last = facts->first;
			}

			if (facts->size == 0)
			{
				facts->first = NULL;
				facts->last = NULL;
			}
		}

		curr = curr->next;
	}
}

FactLinkedListNode* deleteFactCursor(FactLinkedList* facts, FactLinkedListNode* cursor)
{
	FactLinkedListNode* next = NULL;

	if (cursor != facts->last)
	{
		if (cursor == facts->first)
		{
			facts->first = facts->first->next;
			next = facts->first;
			free(cursor);
			facts->size--;
		}
		else
		{
			FactLinkedListNode* prec = facts->first;
			while (prec->next != cursor)
			{
				prec = prec->next;
			}
			prec->next = prec->next->next;
			next = prec->next;
			free(cursor);
			facts->size--;
		}
	}
	else
	{
		if (facts->size == 1)
		{
			free(cursor);
			facts->first = NULL;
			facts->last = NULL;
			facts->size = 0;
		}
		else
		{
			FactLinkedListNode* prec = facts->first;
			while (prec->next != cursor)
			{
				prec = prec->next;
			}
			prec->next = NULL;
			facts->last = prec;
			free(cursor);
			facts->size--;
		}
	}

	if (facts->size == 1)
	{
		facts->last = facts->first;
	}

	if (facts->size == 0)
	{
		facts->first = NULL;
		facts->last = NULL;
	}

	return next;
}