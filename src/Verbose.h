#include <stdbool.h>
#include <string.h>

#ifndef VERBOSE_H_INCLUDED
#define VERBOSE_H_INCLUDED

// ---------------------------------------------- Constants ----------------------------------------------------

// Various syntaxes for the verbose argument.
extern const char* VERBOSE_ARGS[];

// Number of possible syntaxes for the verbose argument.
extern const int NB_VERBOSE_ARGS;

// ---------------------------------------------- Variables ----------------------------------------------------

// Boolean variable indicating if verbose mode is on or off.
extern bool verboseOn;

// ---------------------------------------------- Functions ----------------------------------------------------

// Runs through all arguments and all possible verbose syntaxes to determine if verbose mode is on.
void detectVerbose(int argc, char** argv);

#endif //!VERBOSE_H_INCLUDED